const npc = {1: `pedra`, 2: `papel`, 3: `tesoura`};

const stoneChoice = document.getElementById(`pedra`);
const paperChoice = document.getElementById(`papel`);
const scissorsChoice = document.getElementById(`tesoura`);

const npcChoice = () =>{
    let choice = 0;
    choice = Math.ceil(Math.random() * 3);
    return npc[choice];
}

const npcHand = document.createElement(`img`);
const speakNpc = document.createElement(`p`);
document.getElementById(`content__npc`).appendChild(npcHand);
document.getElementById(`content__npc`).appendChild(speakNpc);

const printChoice = () =>{
    let choice = npcChoice();
    npcHand.setAttribute(`src`, `./imagem/${choice}.png`)
    speakNpc.innerText = `Minha escolha é ${choice}`;
    return choice;
}

const decision = (evt) =>{
    let choice = printChoice();
    let yourChoice = evt.target.id;
    verificationturn(yourChoice, choice);
}

const verificationDraw = (evtId , choice) =>{
    if(evtId === `pedra` && choice === `pedra`){
        setTimeout(function(){ alert("Você empatou"); }, 500);
    }else if(evtId === `papel` && choice === `papel`){
        setTimeout(function(){ alert("Você empatou"); }, 500);
    }else if(evtId === `tesoura` && choice === `tesoura`){
        setTimeout(function(){ alert("Você empatou"); }, 500);
    }
}
const verificationVictory = (evtId , choice) =>{
    if(evtId === `pedra` && choice === `tesoura`){
        setTimeout(function(){ alert("Você Ganhou!"); }, 500);
    }else if(evtId === `papel` && choice === `pedra`){
        setTimeout(function(){ alert("Você Ganhou!"); }, 500);
    }else if(evtId === `tesoura` && choice === `papel`){
        setTimeout(function(){ alert("Você Ganhou!"); }, 500);
    }
}
const verificationLose = (evtId , choice) =>{
    if(evtId === `pedra` && choice === `papel`){
        setTimeout(function(){ alert("Você perdeu =´["); }, 500);
    }else if(evtId === `papel` && choice === `tesoura`){
        setTimeout(function(){ alert("Você perdeu =´["); }, 500);
    }else if(evtId === `tesoura` && choice === `pedra`){
        setTimeout(function(){ alert("Você perdeu =´["); }, 500);
    }
}
const verificationturn = (evtId, choice) =>{
    verificationDraw(evtId, choice);
    verificationLose(evtId, choice);
    verificationVictory(evtId, choice);
}

stoneChoice.addEventListener(`click`, decision);
paperChoice.addEventListener(`click`, decision);
scissorsChoice.addEventListener(`click`, decision);